from minio import Minio
from minio.error import S3Error

client = Minio("172.17.0.1:9000", access_key="testkey", secret_key="testsecret", secure=False)


def main():
    if not client.bucket_exists('test'):
        client.make_bucket('test')
    else:
        print("bucket")


if __name__ == "__main__":
    try:
        main()
    except S3Error as exc:
        print("error occurred.", exc)
